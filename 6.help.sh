#!/bin/bash

echo "Substituição de variável: é a substituição de uma variável pelo seu valor correspondente"

echo "exemplo  name=Arthur
               echo Olá,\$name"

echo "Substituição de shell: é a execução de um comando dentro de um script ou comando de linha de comando"

echo "exemplo  você está no diretório \$((num1 + num2))"


echo "Substituição aritimética: é a execução de uma operação aritmética dentro de um script ou comando de linha de comando"

echo "exemplo: num1=10
               num2=5
	       echo A soma de' \$num1 e  \$num2 é \$((num1+num2))"
